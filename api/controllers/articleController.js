const Article = require('../models').Article;
const {Op} = require('sequelize');

exports.getAll = (req, res, next) => {

    let limit = req.query.limit ?
        (
            isNaN(req.query.limit) ?
                20
                :
                req.query.limit
        )
        : 20;

    let offset = req.query.offset ?
        (
            isNaN(req.query.offset) ?
                0
                :
                req.query.offset
        )
        : 0;

    let title = req.query.s ? req.query.s : "";

    Article.findAll({
        where: {
            title: {
                [Op.like]: "%" + title + "%"
            }
        },
        limit: limit,
        offset: offset,
        order: [
            ['title']
        ],
        raw: true,
        nest: true
    })
        .then(result =>
            result.length > 0 ?
                res.json({
                    code: 200,
                    data: result,
                    offset: offset,
                    limit: limit
                })
                :
                res.json({
                    code: 204,
                    data: "Aucunes données"
                })
        )
        .catch(error => {
            res.json({
                code: 500,
                message: "Une erreur est survenue. "
            });
        });

};

exports.getOne = (req, res, next) => {

    Article.findOne({
        where: {
            id: {
                [Op.eq]: req.params.id
            },
        }
    })
        .then(result => {
                if (result) {
                    res.json({
                        code: 200,
                        data: result
                    });
                } else {
                    res.json({
                        code: 404,
                        data: "Not found"
                    });
                }
            }
        )
        .catch(error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};

exports.getOneNext = (req, res, next) => {

    Article.findOne({
        where: {
            id: {
                [Op.eq]: req.params.id
            },
        }
    })
        .then(result => {
                if (result) {
                    res.locals.article = result.dataValues;
                    next();
                } else {
                    res.json({
                        code: 404,
                        message: "Aucunes correspondances"
                    });
                }
            }
        )
        .catch(error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};

exports.getOneStrict = (articleId) => {

    return Article.findOne({
        where: {
            id: {
                [Op.eq]: articleId
            },
        }
    });

};

exports.create = (req, res, next) => {

    const {authUserCurrent} = res.locals;

    Article.create({
        title: req.body.title,
        body: req.body.body,
        UserId: authUserCurrent.id
    })
        .then(result => {
            res.json({
                code: 301,
                article: result.dataValues
            });
        })
        .catch(error => {
            res.json({
                code: 500,
                message: "Une erreur est survenue. "
            });
        });

};

exports.update = (req, res, next) => {

    const {authUserCurrent} = res.locals;

    Article.findOne({
        where: {
            id: {
                [Op.eq]: req.params.id
            }
        }
    })
        .then(result => {
                if (result) {
                    if (result.dataValues.UserId === authUserCurrent.id) {
                        Article.update(
                            {
                                title: req.body.title,
                                body: req.body.body,
                                UserId: authUserCurrent.id
                            },
                            {
                                where: {
                                    id: {
                                        [Op.eq]: req.params.id
                                    }
                                }
                            })
                            .then(result =>
                                result[0] ?
                                    res.json({
                                        code: 200,
                                        message: "Article mis à jour"
                                    })
                                    :
                                    null
                            )
                            .catch(
                                error => {
                                    res.json({
                                        code: 500,
                                        message: "Une erreur est survenue. "
                                    });
                                }
                            );
                    } else {
                        res.json({
                            code: 401,
                            message: "Cet article n'appartient pas a l'utilisateur connecté"
                        });
                    }
                } else {
                    res.json({
                        code: 404,
                        message: "Aucunes correspondances"
                    });
                }
            }
        )
        .catch(
            error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};

exports.delete = (req, res, next) => {

    const {authUserCurrent} = res.locals;


    Article.findOne({
        where: {
            id: {
                [Op.eq]: req.params.id
            }
        }
    })
        .then(result => {
                if (result) {
                    if (result.dataValues.UserId === authUserCurrent.id) {
                        Article.destroy({
                            where: {
                                id: {
                                    [Op.eq]: req.params.id
                                }
                            }
                        })
                            .then(result =>
                                result ?
                                    res.json({
                                        code: 200,
                                        message: "Article supprimé"
                                    })
                                    :
                                    null
                            )
                            .catch(error => {
                                res.json({
                                    code: 500,
                                    message: "Une erreur est survenue. "
                                });
                            });
                    } else {
                        res.json({
                            code: 401,
                            message: "Cet article n'appartient pas a l'utilisateur connecté"
                        });
                    }
                } else {
                    res.json({
                        code: 404,
                        message: "Aucunes correspondances"
                    });
                }
            }
        )
        .catch(
            error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};