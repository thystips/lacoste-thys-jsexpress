const express = require('express');
const router = express.Router();
const articleController = require('../controllers/articleController');
const commentaireController = require('../controllers/commentaireController');

const auth = require('../middlewares/auth');

// params : limit / offset / s (search by title)
router.get('/',
    articleController.getAll
);

router.get('/:id',
    articleController.getOne
);

router.post('/create',
    auth,
    articleController.create
);

router.delete('/:id/delete',
    auth,
    articleController.delete
);

router.put('/:id/update',
    auth,
    articleController.update
);

router.get('/:id/commentaire',
    articleController.getOneNext,
    commentaireController.getAll
);

router.post('/:id/commentaire',
    auth,
    articleController.getOneNext,
    commentaireController.create
);

router.delete('/:id/commentaire/:idCommentaire',
    auth,
    articleController.getOneNext,
    commentaireController.delete
);

module.exports = router;