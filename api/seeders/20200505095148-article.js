'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Articles', [
      {
        title: 'Article 1',
        body: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vel mi risus. Proin felis lectus, convallis eget quam eu, ornare facilisis ante. Aenean fringilla elementum ante pharetra imperdiet. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc nisl lorem, consequat sit amet urna id, blandit accumsan leo. Nulla facilisi. Quisque ut elementum lorem. Quisque consectetur, purus et consequat porttitor, nulla erat consectetur leo, eget molestie erat magna et libero. Nam vitae tincidunt arcu. Nunc urna diam, ornare nec ultrices ut, eleifend vestibulum est. Nulla mollis felis et ligula rutrum feugiat. Phasellus mollis sem mi, id.`,
        UserId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Article 2',
        body: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus, erat non vehicula gravida, libero lectus tristique odio, sed rutrum nibh diam a eros. Vivamus fermentum, sem eu scelerisque commodo, nisi purus scelerisque tellus, et blandit velit nulla non nibh. Donec sit amet lacus eget ligula finibus rutrum. Pellentesque eleifend libero vulputate condimentum ultricies. Vivamus nec sem accumsan, aliquet lacus vitae, elementum urna. Aliquam vitae metus velit. Nulla tincidunt risus elit, id aliquet nunc rutrum faucibus. Nulla in pharetra lorem. Etiam congue vitae eros eu fringilla. Proin dapibus, risus nec porttitor mollis, dolor elit consectetur nisi, non finibus eros lacus ac justo. Nullam finibus nibh sem, vel eleifend nunc fermentum quis. Suspendisse in sem dignissim, bibendum lorem et, congue tortor. Donec in erat a quam scelerisque suscipit non id neque.

Quisque vulputate ipsum vel tortor consequat, a sagittis nulla hendrerit. Sed a nibh leo. Morbi dictum tellus vel urna euismod consequat. Aenean leo tellus, laoreet quis malesuada in, semper venenatis odio. Curabitur tellus diam, interdum quis lobortis et, condimentum non diam. Nam consectetur leo vel sem elementum maximus. Nullam nec felis non purus vestibulum interdum. Sed eget tellus viverra, venenatis ex at, sodales sem. Quisque viverra lectus nec nulla dapibus, non maximus libero aliquam. In vel elit tristique, ornare tortor sed, laoreet turpis. Aenean neque velit, congue a nisl sit amet, commodo tempor orci. In pharetra, tellus lacinia pellentesque rhoncus, urna felis interdum felis, vitae congue dolor velit et ipsum.

Phasellus tincidunt augue eu nibh tempus finibus. Sed pretium, libero eget suscipit lobortis, nulla arcu viverra odio, ut pharetra ipsum urna sit amet orci. Nulla rutrum dolor at odio commodo, quis convallis erat condimentum. Ut non pretium libero. Nulla velit orci, pharetra nec aliquet eget, rhoncus vitae risus. In turpis lectus, varius ac lobortis convallis, molestie ac mi. Nulla facilisi. In ac turpis et ipsum sodales accumsan at dapibus lorem. In ac felis purus. Sed a libero suscipit, vestibulum ex eu, faucibus massa. Pellentesque eget risus quis odio commodo bibendum at ut purus. Duis et varius metus. Fusce vitae volutpat erat. Integer eget commodo sapien, id pretium dolor.

Donec gravida diam vitae commodo sodales. Donec facilisis ligula mauris, id scelerisque lacus auctor ac. Nulla erat velit, egestas a leo eget, iaculis porttitor justo. Phasellus dictum porta dictum. Nullam eu purus in ligula cursus semper. Maecenas nec tortor arcu. Cras augue nunc, ornare vel aliquet a, maximus eu tellus. Nullam accumsan diam et metus scelerisque, ut vehicula dui rutrum. Nam efficitur dui quam, non vehicula arcu luctus eget. Proin sit amet pharetra massa, non scelerisque felis. Maecenas eget erat egestas, porta purus id, convallis quam. Ut gravida ligula vel iaculis bibendum. Proin vestibulum et enim a scelerisque. Fusce egestas nulla orci, id accumsan turpis volutpat ornare. Donec suscipit molestie eros nec fringilla. Quisque lacinia hendrerit odio et elementum.

Donec efficitur, massa vitae varius vestibulum, nisl libero convallis dolor, non euismod dolor nisi sed nisl. Nulla eleifend efficitur nulla, nec dictum nisl viverra vitae. Vivamus sed suscipit enim. Fusce elementum mauris quis maximus mollis. Fusce condimentum elit nec sollicitudin ullamcorper. Suspendisse non erat condimentum, vehicula lorem vel, eleifend velit. Ut sed enim id urna rutrum auctor. In rhoncus libero ante, eget posuere felis imperdiet quis. Quisque quis malesuada orci, nec iaculis nunc. Curabitur luctus laoreet lorem, id tempus justo cursus vel.

Maecenas porta nibh id tortor congue feugiat. Sed iaculis, purus eget malesuada mollis, urna leo semper nisl, ac condimentum elit nibh id augue. Mauris vel imperdiet urna, non bibendum metus. Donec id quam et odio molestie auctor ac maximus augue. Nam aliquet mollis mollis. Sed faucibus nisl eget ante ullamcorper posuere.`,
        UserId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Article 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra justo eget mattis porttitor.',
        body: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed faucibus, erat non vehicula gravida, libero lectus tristique odio, sed rutrum nibh diam a eros. Vivamus fermentum, sem eu scelerisque commodo, nisi purus scelerisque tellus, et blandit velit nulla non nibh. Donec sit amet lacus eget ligula finibus rutrum. Pellentesque eleifend libero vulputate condimentum ultricies. Vivamus nec sem accumsan, aliquet lacus vitae, elementum urna. Aliquam vitae metus velit. Nulla tincidunt risus elit, id aliquet nunc rutrum faucibus. Nulla in pharetra lorem. Etiam congue vitae eros eu fringilla. Proin dapibus, risus nec porttitor mollis, dolor elit consectetur nisi, non finibus eros lacus ac justo. Nullam finibus nibh sem, vel eleifend nunc fermentum quis. Suspendisse in sem dignissim, bibendum lorem et, congue tortor. Donec in erat a quam scelerisque suscipit non id neque.

Quisque vulputate ipsum vel tortor consequat, a sagittis nulla hendrerit. Sed a nibh leo. Morbi dictum tellus vel urna euismod consequat. Aenean leo tellus, laoreet quis malesuada in, semper venenatis odio. Curabitur tellus diam, interdum quis lobortis et, condimentum non diam. Nam consectetur leo vel sem elementum maximus. Nullam nec felis non purus vestibulum interdum. Sed eget tellus viverra, venenatis ex at, sodales sem. Quisque viverra lectus nec nulla dapibus, non maximus libero aliquam. In vel elit tristique, ornare tortor sed, laoreet turpis. Aenean neque velit, congue a nisl sit amet, commodo tempor orci. In pharetra, tellus lacinia pellentesque rhoncus, urna felis interdum felis, vitae congue dolor velit et ipsum.

Phasellus tincidunt augue eu nibh tempus finibus. Sed pretium, libero eget suscipit lobortis, nulla arcu viverra odio, ut pharetra ipsum urna sit amet orci. Nulla rutrum dolor at odio commodo, quis convallis erat condimentum. Ut non pretium libero. Nulla velit orci, pharetra nec aliquet eget, rhoncus vitae risus. In turpis lectus, varius ac lobortis convallis, molestie ac mi. Nulla facilisi. In ac turpis et ipsum sodales accumsan at dapibus lorem. In ac felis purus. Sed a libero suscipit, vestibulum ex eu, faucibus massa. Pellentesque eget risus quis odio commodo bibendum at ut purus. Duis et varius metus. Fusce vitae volutpat erat. Integer eget commodo sapien, id pretium dolor.

Donec gravida diam vitae commodo sodales. Donec facilisis ligula mauris, id scelerisque lacus auctor ac. Nulla erat velit, egestas a leo eget, iaculis porttitor justo. Phasellus dictum porta dictum. Nullam eu purus in ligula cursus semper. Maecenas nec tortor arcu. Cras augue nunc, ornare vel aliquet a, maximus eu tellus. Nullam accumsan diam et metus scelerisque, ut vehicula dui rutrum. Nam efficitur dui quam, non vehicula arcu luctus eget. Proin sit amet pharetra massa, non scelerisque felis. Maecenas eget erat egestas, porta purus id, convallis quam. Ut gravida ligula vel iaculis bibendum. Proin vestibulum et enim a scelerisque. Fusce egestas nulla orci, id accumsan turpis volutpat ornare. Donec suscipit molestie eros nec fringilla. Quisque lacinia hendrerit odio et elementum.

Donec efficitur, massa vitae varius vestibulum, nisl libero convallis dolor, non euismod dolor nisi sed nisl. Nulla eleifend efficitur nulla, nec dictum nisl viverra vitae. Vivamus sed suscipit enim. Fusce elementum mauris quis maximus mollis. Fusce condimentum elit nec sollicitudin ullamcorper. Suspendisse non erat condimentum, vehicula lorem vel, eleifend velit. Ut sed enim id urna rutrum auctor. In rhoncus libero ante, eget posuere felis imperdiet quis. Quisque quis malesuada orci, nec iaculis nunc. Curabitur luctus laoreet lorem, id tempus justo cursus vel.

Maecenas porta nibh id tortor congue feugiat. Sed iaculis, purus eget malesuada mollis, urna leo semper nisl, ac condimentum elit nibh id augue. Mauris vel imperdiet urna, non bibendum metus. Donec id quam et odio molestie auctor ac maximus augue. Nam aliquet mollis mollis. Sed faucibus nisl eget ante ullamcorper posuere.`,
        UserId: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Articles', null, {});
  }
};
