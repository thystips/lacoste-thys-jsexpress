'use strict';
module.exports = (sequelize, DataTypes) => {
    const Article = sequelize.define('Article', {
        title: DataTypes.STRING,
        body: DataTypes.STRING,
        UserId: DataTypes.INTEGER
    }, {});
    Article.associate = function (models) {
        Article.hasMany(models.Commentaire, {as: 'commentaire'});
        Article.belongsTo(models.User, {foreignKey: "UserId", as: "author"});
    };
    return Article;
};