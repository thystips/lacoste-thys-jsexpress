import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-article-card',
    templateUrl: './article-card.component.html',
    styleUrls: ['./article-card.component.scss']
})
export class ArticleCardComponent implements OnInit {

    @Input() id: string;
    @Input() title: string;
    @Input() content: string;

    constructor() {
    }

    ngOnInit(): void {

        const deleteCount = Math.round(this.title.length / 10);

        if (deleteCount >= 10) {
            this.content = null;
        } else {
            this.content = this.content.split('').splice(0, 240 - (30 * deleteCount)).join('') + '...';
        }

    }

}

// 268
