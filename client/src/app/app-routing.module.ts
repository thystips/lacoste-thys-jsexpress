import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ArticleListComponent} from './components/article-list/article-list.component';
import {ArticleReadComponent} from './components/article-read/article-read.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {ReverseAuthGuard} from './routes/guards/auth/reverse-auth.guard';
import {ArticleCreateComponent} from './components/article-create/article-create.component';
import {AuthGuard} from './routes/guards/auth/auth.guard';


const routes: Routes = [
    {
        path: '',
        redirectTo: '/feed',
        pathMatch: 'full'
    },
    {
        path: 'feed',
        component: ArticleListComponent
    },
    {
        path: 'article/new',
        component: ArticleCreateComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'article/:id',
        component: ArticleReadComponent
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [ReverseAuthGuard]
    },
    {
        path: 'register',
        component: RegisterComponent,
        canActivate: [ReverseAuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
