import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ArticleListComponent} from './components/article-list/article-list.component';
import {ArticleCardComponent} from './components/article-card/article-card.component';
import {ArticleReadComponent} from './components/article-read/article-read.component';
import {HeaderComponent} from './components/header/header.component';
import {LoginComponent} from './components/login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './services/auth/auth.service';
import {HttpClientModule} from '@angular/common/http';
import {RegisterComponent} from './components/register/register.component';
import {CookieService} from "ngx-cookie-service";
import {ArticleService} from "./services/article/article.service";
import { CommentaireCardComponent } from './components/commentaire-card/commentaire-card.component';
import { ArticleCreateComponent } from './components/article-create/article-create.component';

@NgModule({
    declarations: [
        AppComponent,
        ArticleListComponent,
        ArticleCardComponent,
        ArticleReadComponent,
        HeaderComponent,
        LoginComponent,
        RegisterComponent,
        CommentaireCardComponent,
        ArticleCreateComponent
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule
    ],
    providers: [
        AuthService,
        CookieService,
        ArticleService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
