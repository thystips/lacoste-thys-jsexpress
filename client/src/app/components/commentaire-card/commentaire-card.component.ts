import {Component, Input, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {ArticleService} from "../../services/article/article.service";

@Component({
    selector: 'app-commentaire-card',
    templateUrl: './commentaire-card.component.html',
    styleUrls: ['./commentaire-card.component.scss']
})
export class CommentaireCardComponent implements OnInit {

    @Input() body;
    @Input() authorId;
    @Input() articleAuthorId;
    @Input() idCom;
    @Input() articleId;
    author: string;

    constructor(public auth: AuthService, private articleService: ArticleService) {
    }

    ngOnInit(): void {
        this.auth.get(this.authorId).subscribe(
            value => {
                this.author = value.data.username;
            }
        );
    }

    deleteCom(comId) {
        this.articleService.delCommentaire(this.articleId, comId).subscribe(value => value, error => error, () => location.reload());
    }

}
