import {Component, OnInit} from '@angular/core';
import {ArticleService} from '../../services/article/article.service';

declare let $: any;

@Component({
    selector: 'app-article-list',
    templateUrl: './article-list.component.html',
    styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements OnInit {

    articles: Array<any> = [];

    constructor(private articleService: ArticleService) {
    }

    ngOnInit(): void {

        this.articleService.getAll().subscribe(
            value => {

                for (const article of value.data) {
                    this.articles.push(article);
                }

                console.log(this.articles);

            },
            error => {
                console
                    .log(error);
            }
        )
        ;

    }

}
