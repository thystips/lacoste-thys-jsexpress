const {Sequelize} = require("sequelize");

const db = new Sequelize({
    dialect: 'sqlite',
    storage: 'database.sqlite'
});

try {
    db.authenticate();
    console.log("DB OK !");
} catch (e) {
    console.error("DB NOT OK : " + e);
}

module.exports = db;
