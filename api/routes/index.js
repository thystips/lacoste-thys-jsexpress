const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {
        title: 'Express',
        message: 'Page en GET'
    });
});
router.post('/', function (req, res, next) {
    res.render('index', {
        title: 'Express',
        message: 'Page en POST'
    });
});

module.exports = router;
