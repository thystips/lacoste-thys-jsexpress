import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpParams} from '@angular/common/http';
import {AuthService} from '../../services/auth/auth.service';

declare let $: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    buttonLoading = false;
    responseCode: number;

    constructor(private fb: FormBuilder, public auth: AuthService) {
    }

    ngOnInit(): void {

        console.log(this.auth.isLoggedIn);

        this.loginForm = this.fb.group({
            username: [
                '',
                [
                    Validators.required
                ]
            ],
            password: [
                '',
                [
                    Validators.required
                ]
            ]
        });

    }

    get form() {
        return this.loginForm.controls;
    }

    get formEncoded() {
        return new HttpParams()
            .set('username', this.form.username.value)
            .set('password', this.form.password.value)
            .toString();
    }

    submit() {
        this.btnToggleLoader();
        this.auth.login(this.formEncoded).subscribe(
            value => {
                console.log(value);
                this.auth.manualSetUser(value.user);
                this.responseCode = value.code;
                location.reload();
            },
            error => {
                console.log(error);
            },
            () => {
                this.btnToggleLoader();
                // location.reload();
            }
        );
    }

    btnToggleLoader() {
        if (!this.buttonLoading) {
            $('button.btn.btn-primary.w-50').html(`

            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
            Connexion...

            `);
            this.buttonLoading = !this.buttonLoading;
        } else {
            $('button.btn.btn-primary.w-50').html(`Se connecter`);
            this.buttonLoading = !this.buttonLoading;
        }
    }

}
