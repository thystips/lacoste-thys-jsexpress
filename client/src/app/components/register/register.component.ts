import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth/auth.service';
import {HttpParams} from '@angular/common/http';

declare let $: any;

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    registerForm: FormGroup;
    buttonLoading = false;
    responseCode: number;

    constructor(private fb: FormBuilder, private auth: AuthService) {
    }

    ngOnInit(): void {

        this.registerForm = this.fb.group({
            username: [
                '',
                [
                    Validators.required
                ]
            ],
            password: [
                '',
                [
                    Validators.required
                ]
            ]
        });

    }

    get form() {
        return this.registerForm.controls;
    }

    get formEncoded() {

        return new HttpParams()
            .set('username', this.form.username.value)
            .set('password', this.form.password.value)
            .toString();
    }

    submit() {

        this.btnToggleLoader();

        this.auth.register(this.formEncoded).subscribe(
            value => {
                this.responseCode = value.code;
            },
            error => {
                console.log(error);
            },
            () => {
                this.btnToggleLoader();
            }
        );
    }

    btnToggleLoader() {
        if (!this.buttonLoading) {
            $('button.btn.btn-primary.w-50').html(`

            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
            Connexion...

            `);
            this.buttonLoading = !this.buttonLoading;
        } else {
            $('button.btn.btn-primary.w-50').html(`Se connecter`);
            this.buttonLoading = !this.buttonLoading;
        }
    }

}
