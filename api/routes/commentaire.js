const express = require('express');
const router = express.Router();
const commentaireController = require('../controllers/commentaireController');

const auth = require('../middlewares/auth');

router.get('/:id',
    commentaireController.getOne
);

module.exports = router;