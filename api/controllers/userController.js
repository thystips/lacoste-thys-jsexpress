const User = require('../models').User;
const bcrypt = require('bcrypt');
const {Op} = require('sequelize');
const jwt = require('jsonwebtoken');

exports.getAll = (req, res, next) => {

    let limit = req.query.limit ?
        (
            isNaN(req.query.limit) ?
                20
                :
                req.query.limit
        )
        : 20;


    let offset = req.query.offset ?
        (
            isNaN(req.query.offset) ?
                0
                :
                req.query.offset
        )
        : 0;

    let username = req.query.s ? req.query.s : "";


    User.findAll({
        where: {
            username: {
                [Op.like]: "%" + username + "%"
            }
        },
        limit: limit,
        offset: offset,
        order: [
            ['username']
        ],
        raw: true,
        nest: true
    })
        .then(result =>
            result.length > 0 ?
                res.json({
                    code: 200,
                    data: result,
                    offset: offset,
                    limit: limit
                })
                :
                res.json({
                    code: 204,
                    message: "Aucunes données"
                })
        )
        .catch(error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};

exports.get = (req, res, next) => {

    User.findOne({
        where: {
            id: {
                [Op.eq]: req.params.id
            }
        }
    })
        .then(result =>
            result ?
                res.json({
                    code: 200,
                    data: result
                })
                :
                res.json({
                    code: 404,
                    message: "Aucunes correspondances"
                })
        )
        .catch(error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};

exports.getOne = (userId) => {

    return User.findOne({
        where: {
            id: {
                [Op.eq]: userId
            }
        }
    });

};

exports.create = (req, res, next) => {

    let password = bcrypt.hashSync(req.body.password, 5);

    User.create({
        username: req.body.username,
        password: password
    })
        .then(result => {
            res.json({
                code: 301,
                user: result.dataValues
            });
        })
        .catch(error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};

exports.delete = (req, res, next) => {

    const {authUserCurrent} = res.locals;

    User.destroy({
        where: {
            id: {
                [Op.eq]: authUserCurrent.id
            }
        }
    })
        .then(result =>
            res.json({
                code: 200,
                message: "Utilisateur supprimé"
            })
        )
        .catch(error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};

exports.update = (req, res, next) => {

    const {authUserCurrent} = res.locals;
    let password = bcrypt.hashSync(req.body.password, 5);

    User.update(
        {
            username: req.body.username,
            password: password
        },
        {
            where: {
                id: {
                    [Op.eq]: authUserCurrent.id
                }
            }
        })
        .then(result =>
            result[0] ?
                res.json({
                    code: 200,
                    message: "Utilisateur mis à jour"
                })
                :
                res.json({
                    code: 404,
                    message: "Aucunes correspondances"
                })
        )
        .catch(error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );
};

exports.login = (req, res, next) => {

    User.findOne({
        limit: 1,
        where: {
            username: {
                [Op.eq]: req.body.username
            }
        }
    })
        .then(result => {
            if (result) {
                if (bcrypt.compareSync(req.body.password, result.password)) {
                    const token = jwt.sign(
                        {
                            userId: result.id
                        },
                        'RANDOM_TOKEN_SECRET',
                        {
                            expiresIn: '24h'
                        }
                    );
                    res.cookie('authToken', token, {maxAge: 3600 * 24 * 1000, httpOnly: true});
                    res.json({
                        code: 200,
                        message: "Utilisateur authentifié",
                        user: result,
                        token: token
                    });
                } else {
                    res.json({
                        code: 403,
                        message: "Mauvais mot de passe"
                    });
                }
            } else {
                res.json({
                    code: 404,
                    message: "Aucunes correspondances"
                });
            }
        })
        .catch(
            error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};

exports.current = (req, res, next) => {

    const token = req.cookies.authToken;
    const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
    const userId = decodedToken.userId;

    User.findOne({
        where: {
            id: {
                [Op.eq]: userId
            }
        }
    })
        .then(result =>
            result ?
                res.json({
                    code: 200,
                    data: result
                })
                :
                res.json({
                    code: 404,
                    message: "Aucunes correpondances"
                })
        )
        .catch(error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};

exports.logout = (req, res, next) => {

    res.clearCookie('authToken');
    res.json(
        {
            code: 200,
            message: "Utilisateur déconnecté"
        }
    );

};

exports.logoutNext = (req, res, next) => {

    res.clearCookie('authToken');
    next();

};
