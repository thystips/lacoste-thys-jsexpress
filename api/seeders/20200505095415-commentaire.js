'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Commentaires', [
      {
        body: 'Lorem ipsum dolor sit amet',
        UserId: 1,
        ArticleId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        body: 'Mauris tempus pretium lectus, et suscipit dolor finibus vitae. Duis tincidunt lectus vel molestie mattis.',
        UserId: 2,
        ArticleId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        body: 'pellentesque. Aliquam eleifend metus sit amet magna feugiat ultrice',
        UserId: 3,
        ArticleId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        body: 'Nullam id lectus lobortis libero finibus porttitor. Duis bibendum elit ac auctor pretium.',
        UserId: 1,
        ArticleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        body: 'Maecenas nec aliquet nulla, eu dignissim tellus. Sed auctor est id vehicula dictum. ',
        UserId: 2,
        ArticleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        body: 'Aliquam ac sollicitudin sem.',
        UserId: 3,
        ArticleId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        body: 'Donec sagittis purus',
        UserId: 1,
        ArticleId: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        body: 'Sed ultrices vel erat faucibus blandit.',
        UserId: 2,
        ArticleId: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        body: 'Donec sit amet ullamcorper metus, non blandit dolor.',
        UserId: 3,
        ArticleId: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Commentaires', null, {});
  }
};
