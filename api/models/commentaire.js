'use strict';
module.exports = (sequelize, DataTypes) => {
    const Commentaire = sequelize.define('Commentaire', {
        body: DataTypes.STRING,
        UserId: DataTypes.INTEGER,
        ArticleId: DataTypes.INTEGER
    }, {});
    Commentaire.associate = function (models) {
        Commentaire.belongsTo(models.User, {foreignKey: "UserId", as: "user"});
        Commentaire.belongsTo(models.Article, {foreignKey: "ArticleId", as: "article"});
    };
    return Commentaire;
};
