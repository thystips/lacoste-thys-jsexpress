const models = require('./models');
const User = models.User;
const Article = models.Article;
const Commentaire = models.Commentaire;
const seq = require('sequelize');

User.bulkCreate([
    {username: "Antoine_thys", password: "password"},
    {username: "Louis_lacoste", password: "password"}
])
    .then(users => {
        console.log("De nouveaux users ont été créées !");
        // console.log(users);
    })
    .catch(error => {
        console.error("Une erreur est survenue : " + error);
    });

Article.create({
    title: "Le premier article de Antoine Thys !",
    body: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur dignissimos dolorum odio vero ea ipsam voluptatem aperiam laudantium ipsa, beatae, dicta quas. Pariatur praesentium reprehenderit omnis distinctio, fuga odio nihil?Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus repudiandae molestiae, enim alias hic unde deserunt, accusantium, quia aliquam fuga a. Dolore quos, suscipit voluptatum illo iste harum odio sapiente!",
    user_id: 1
})
    .then(article => {
        console.log("Nouvel article !");
        // console.log(article);
    })
    .catch(error => {
        console.error("Une erreur est survenue : " + error);
    });

Commentaire.create({
    body: "Je trouve cet article trés bien !",
    user_id: 2,
    article_id: 1
})
    .then(commentaire => {
        console.log("Nouveau commentaire !");
        // console.log(commentaire);
    })
    .catch(error => {
        console.error("Une erreur est survenue : " + error);
    });

Article.findAll({
    where: {
        author: {
            [seq.Op.eq]: 1
        }
    }
}).then(article => console.log(article));