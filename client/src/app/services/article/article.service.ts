import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ArticleService {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        })
    };

    constructor(private http: HttpClient) {
    }

    getAll(page = 0): Observable<any> {
        return this.http.get(environment.Api.url + 'article?offset=' + (page * 20) + '&limit=' + ((page + 1) * 20), this.httpOptions);
    }

    get(articleId): Observable<any> {
        return this.http.get(environment.Api.url + 'article/' + articleId, this.httpOptions);
    }

    getCommentaires(articleId): Observable<any> {
        return this.http.get(environment.Api.url + 'article/' + articleId + '/commentaire', this.httpOptions);
    }

    delCommentaire(articleId, commetnaireId): Observable<any> {
        return this.http.delete(environment.Api.url + 'article/' + articleId + '/commentaire/' + commetnaireId, this.httpOptions);
    }

    commenter(articleId, val): Observable<any> {
        return this.http.post(environment.Api.url + 'article/' + articleId + '/commentaire', val, this.httpOptions);
    }

    create(val): Observable<any> {
        return this.http.post(environment.Api.url + 'article/create', val, this.httpOptions);
    }

}
