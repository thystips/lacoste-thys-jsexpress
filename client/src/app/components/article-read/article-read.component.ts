import {Component, OnInit} from '@angular/core';
import {ArticleService} from '../../services/article/article.service';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth/auth.service';
import {HttpParams} from '@angular/common/http';

declare let $: any;

@Component({
    selector: 'app-article-read',
    templateUrl: './article-read.component.html',
    styleUrls: ['./article-read.component.scss']
})
export class ArticleReadComponent implements OnInit {

    article: any;
    articleId: string = this.router.snapshot.paramMap.get('id');
    commentaireForm: FormGroup;
    commentaires: Array<any> = [];
    private buttonLoading: boolean;

    constructor(private articleService: ArticleService, private router: ActivatedRoute, private fb: FormBuilder, public auth: AuthService) {
    }

    ngOnInit(): void {
        this.commentaireForm = this.fb.group({
            commentaire: [
                '',
                [
                    Validators.required
                ]
            ]
        });

        this.articleService.get(this.articleId).subscribe(
            value => {
                this.article = value.data;
            }
        );

        this.articleService.getCommentaires(this.articleId).subscribe(
            value => {
                for (const element of value.data) {
                    this.commentaires.push(element);
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    get form() {
        return this.commentaireForm.controls;
    }

    get formEncoded() {
        return new HttpParams().set('body', this.form.commentaire.value).toString();
    }

    submit(): void {
        this.btnToggleLoader();
        this.articleService.commenter(this.articleId, this.formEncoded).subscribe(
            value => {
            },
            error => {
                console.log(error);
            },
            () => {
                this.btnToggleLoader();
                this.articleService.getCommentaires(this.articleId).subscribe(
                    value => {
                        this.commentaires = [];
                        for (const element of value.data) {
                            this.commentaires.push(element);
                        }
                    },
                    error => {
                        console.log(error);
                    }
                );
            }
        );
    }

    btnToggleLoader() {
        if (!this.buttonLoading) {
            $('button.btn.btn-primary.w-50').html(`

            <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
            Connexion...

            `);
            this.buttonLoading = !this.buttonLoading;
        } else {
            $('button.btn.btn-primary.w-50').html(`Se connecter`);
            this.buttonLoading = !this.buttonLoading;
        }
    }

}
