import {Component, OnInit} from '@angular/core';
import {ArticleService} from '../../services/article/article.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpParams} from "@angular/common/http";


@Component({
    selector: 'app-article-create',
    templateUrl: './article-create.component.html',
    styleUrls: ['./article-create.component.scss']
})
export class ArticleCreateComponent implements OnInit {

    articleForm: FormGroup;

    constructor(private articleService: ArticleService, private router: Router, private fb: FormBuilder) {
    }

    ngOnInit(): void {
        this.articleForm = this.fb.group({
            title: [
                '',
                [
                    Validators.required
                ]
            ],
            body: [
                '',
                [
                    Validators.required
                ]
            ]
        });
    }

    get form() {
        return this.articleForm.controls;
    }

    get formEncoded() {
        return new HttpParams()
            .set('title', this.form.title.value)
            .set('body', this.form.body.value)
            .toString();
    }

    submit() {
        this.articleService.create(this.formEncoded).subscribe(
            value => {
                console.log(value);
            },
            error => {
                console.log(error);
            },
            () => {
                this.router.navigateByUrl('/feed');
            }
        );
    }

}
