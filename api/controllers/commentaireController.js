const Commentaire = require('../models').Commentaire;
const articleController = require('./articleController');
const {Op} = require('sequelize');

exports.getAll = (req, res, next) => {

    let limit = req.query.limit ?
        (
            isNaN(req.query.limit) ?
                20
                :
                req.query.limit
        )
        : 20;

    let offset = req.query.offset ?
        (
            isNaN(req.query.offset) ?
                0
                :
                req.query.offset
        )
        : 0;

    let title = req.query.s ? req.query.s : "";

    Commentaire.findAll({
        where: {
            ArticleId: {
                [Op.eq]: req.params.id
            }
        },
        limit: limit,
        offset: offset,
        order: [
            ['id']
        ],
        raw: true,
        nest: true
    })
        .then(result =>
            result.length > 0 ?
                res.json({
                    code: 200,
                    data: result,
                    offset: offset,
                    limit: limit
                })
                :
                res.json({
                    code: 204,
                    message: "Aucunes données"
                })
        )
        .catch(error => {
            res.json({
                code: 500,
                message: "Une erreur est survenue."
            });
        });

};

exports.getOne = (req, res, next) => {

    Commentaire.findOne({
        where: {
            id: {
                [Op.eq]: req.params.id
            },
        }
    })
        .then(result =>
            result ?
                res.json({
                    code: 200,
                    data: result
                })
                :
                res.json({
                    code: 404,
                    message: "Aucunes correspondances"
                })
        )
        .catch(error => res.json({
            code: 500,
            message: "Une erreur est survenue."
        }));

};

exports.create = (req, res, next) => {

    const {authUserCurrent, article} = res.locals;

    Commentaire.create({
        body: req.body.body,
        UserId: authUserCurrent.id,
        ArticleId: article.id
    })
        .then(result => {
            res.json({
                code: 301,
                article: result.dataValues
            });
        })
        .catch(error => {
            res.json({
                code: 500,
                message: "Une erreur est survenue. " + error
            });
        });
};

exports.delete = (req, res, next) => {

    const {authUserCurrent, article} = res.locals;

    console.log(authUserCurrent, article, req.params);

    Commentaire.findOne({
        where: {
            id: {
                [Op.eq]: req.params.idCommentaire
            }
        }
    })
        .then(result => {
                if (result) {
                    if (result.dataValues.UserId === authUserCurrent.id) {
                        Commentaire.destroy({
                            where: {
                                id: {
                                    [Op.eq]: req.params.idCommentaire
                                }
                            }
                        })
                            .then(result =>
                                result ?
                                    res.json({
                                        code: 200,
                                        message: "Commentaire supprimé"
                                    })
                                    :
                                    null
                            )
                            .catch(error => {
                                res.json({
                                    code: 500,
                                    message: "Une erreur est survenue. "
                                });
                            });
                    } else if (article.UserId === authUserCurrent.id) {
                        Commentaire.destroy({
                            where: {
                                id: {
                                    [Op.eq]: req.params.idCommentaire
                                }
                            }
                        })
                            .then(result =>
                                result ?
                                    res.json({
                                        code: 200,
                                        message: "Commentaire supprimé"
                                    })
                                    :
                                    null
                            )
                            .catch(error => {
                                res.json({
                                    code: 500,
                                    message: "Une erreur est survenue. "
                                });
                            });
                    } else {
                        res.json({
                            code: 401,
                            message: "Vous ne pouvez pas supprimer ce commentaire"
                        });
                    }
                } else {
                    res.json({
                        code: 404,
                        message: "Aucunes correspondances"
                    });
                }
            }
        )
        .catch(
            error => res.json({
                code: 500,
                message: "Une erreur est survenue. "
            })
        );

};
