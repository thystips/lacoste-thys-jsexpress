import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    constructor(public auth: AuthService, public router: Router) {
    }

    ngOnInit(): void {

        console.log(this.auth.isLoggedIn, JSON.parse(localStorage.getItem('user')));

    }

    logout() {

        this.auth.logout().subscribe(
            value => {
                localStorage.removeItem('user');
                location.reload();
            },
            error => console.log(error)
        );

    }

}
