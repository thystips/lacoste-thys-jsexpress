'use strict';

const bcrypt = require('bcrypt');

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Users', [
          {
            username: 'User1',
            password: bcrypt.hashSync('P@ssw0rd', 5),
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            username: 'User2',
            password: bcrypt.hashSync('P@ssw0rd', 5),
            createdAt: new Date(),
            updatedAt: new Date()
          },
          {
            username: 'User3',
            password: bcrypt.hashSync('P@ssw0rd', 5),
            createdAt: new Date(),
            updatedAt: new Date()
          }
      ], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Users', null, {});
  }
};
